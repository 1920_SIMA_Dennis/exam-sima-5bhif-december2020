﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Exam_SIMA.Domain;
using Exam_SIMA.Infrastructure;
using Exam_SIMA.Dto;
using Microsoft.EntityFrameworkCore;
using Bogus;

namespace Exam_SIMA.api.Service
{
    public class StatisticService
    {
        private readonly TestungContext _context;

        public StatisticService(TestungContext context)
        {
            _context = context;
        }

        public Object Statistic1(DateTime from, DateTime to)
        {
            return _context.Examinations.Where(s => s.ScheduledTS >= from && s.ScheduledTS <= to).Include(a => a.Testee)
            .GroupBy(c => new
            {
              c.ScheduledTS.Date,
              c.Testee.Gender,
              c.Location.City, // for better Test-results-> the city
            })
            .Select(stat => new {
                Date = stat.Key.Date,
                Gender = stat.Key.Gender,
                Location = stat.Key.City,
                Positive = stat.Count(x => x.Result == ExaminationResult.Positive),
                Negative = stat.Count(x => x.Result == ExaminationResult.Negative),
                Invalid = stat.Count(x => x.Result == ExaminationResult.Invald),
            });
        }

        public Object Statistic2(DateTime from, DateTime to)
        {
            return _context.Examinations.Where(s => s.ScheduledTS >= from && s.ScheduledTS <= to && s.ExaminationTS != null)
            .GroupBy(c => new
            {
                c.ExaminationTS.Value.Date
            })
            .Select(stat => new {
                Date = stat.Key.Date,
                Invalid = stat.Count(x => x.Result == ExaminationResult.Invald),
            });
        }

        public Object Statistic3(DateTime from, DateTime to)
        {
            return _context.Examinations.Where(s => s.ScheduledTS >= from && s.ScheduledTS <= to && s.Result != null)
            .GroupBy(c => new
            {
                c.ScheduledTS.Date,
                c.Location.City, 
                c.Result
            })
                        .Select(stat => new {
                            Date = stat.Key.Date,
                            Location = stat.Key.City,
                            Positive = stat.Count(x => x.Result == ExaminationResult.Positive),
                            Negative = stat.Count(x => x.Result == ExaminationResult.Negative),
                            Percentage_of_positive = PercetageCalculator(stat.Count(x => x.Result == ExaminationResult.Positive), stat.Count(x => x.Result == ExaminationResult.Negative)),
                        });
 
        }

        public static double PercetageCalculator(int anteil, int grundwert)
        {
            if (grundwert == 0)
                return anteil * 100;
            else
                return (anteil / grundwert) * 100;
        }
    }
}
