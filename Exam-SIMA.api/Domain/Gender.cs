﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam_SIMA.Domain
{
    public enum Gender
    {
        Male,
        Female,
        Diverse
    }
}
