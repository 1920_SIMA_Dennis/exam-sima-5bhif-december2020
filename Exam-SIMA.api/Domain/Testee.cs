﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam_SIMA.Domain
{
    public class Testee : Person
    {
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public PhoneNumber PhoneNumber { get; set; }
        public int? CurrentPin { get; set; }
        public List<Examination> Examinations { get; set; }
    }
}
