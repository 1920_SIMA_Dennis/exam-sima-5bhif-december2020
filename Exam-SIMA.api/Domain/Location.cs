﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam_SIMA.Domain
{
    public class Location
    {
        public string StreetNo { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public string getFullLocation()
        {
            return StreetNo + " " + ZipCode + " " + City + " " + Country;
        }
    } 
}
