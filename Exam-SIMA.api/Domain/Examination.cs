﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam_SIMA.Domain
{
    public class Examination
    {
        public int Id { get; set; }

        public DateTime ScheduledTS { get; set; }

        public DateTime? ExaminationTS { get; set; }

        public Tester Tester { get; set; }

        public Testee Testee { get; set; }

        public string Token { get; set; }

        public string TestStation { get; set; }

        public int? Value { get; set; }

        public ExaminationResult? Result { get; set; }

        public Location Location { get; set; }
    }
}
