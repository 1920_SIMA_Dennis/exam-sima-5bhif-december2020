﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam_SIMA.Domain
{
    public class PhoneNumber 
    {
        public string CountryCode { get; set; }
        public int AreaCode { get; set; }
        public string SerialNumber { get; set; }


        public string getFullPhone()
        {
            return CountryCode + " " + AreaCode + " "+ SerialNumber;
        }
    }
}
