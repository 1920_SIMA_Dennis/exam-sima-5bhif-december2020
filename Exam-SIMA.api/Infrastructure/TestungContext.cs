﻿using Bogus;
using Bogus.Distributions.Gaussian;
using Bogus.Extensions;
using Exam_SIMA.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exam_SIMA.Infrastructure
{
    public class TestungContext : DbContext
    {
        public DbSet<Testee> Testees { get; set; }
        public DbSet<Tester> Testers { get; set; }
        public DbSet<Examination> Examinations { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=Testung.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Examination>()
                .HasOne(a => a.Testee).WithMany(s => s.Examinations)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Testee>().OwnsOne(t => t.PhoneNumber);
            modelBuilder.Entity<Location>().HasKey(t => new { t.StreetNo, t.ZipCode });
            modelBuilder.Entity<Domain.Person>()
           .HasDiscriminator<string>("PersonType");
        }

        public void Seed()
        {

            Randomizer.Seed = new Random(5);

            // Tester
            var testerFaker = new Faker<Tester>()
                .Rules((f, t) =>
                {
                    t.FirstName = f.Name.FirstName();
                    t.LastName = f.Name.LastName();
                    t.SocialSecurityNumber = f.Random.AlphaNumeric(50);
                    t.Gender = f.PickRandom<Gender>();
                });
            var tester = testerFaker.Generate(20);
            Testers.AddRange(tester);
            SaveChanges();

            //Location - only a few needed for better statistic results
            var locationFaker = new Faker<Location>()
                                            .Rules((f, l) =>
                                            {
                                                l.Country = "Austria"; //only austria
                                                l.City = f.Address.City();
                                                l.StreetNo = f.Address.StreetName();
                                                l.ZipCode = f.Address.ZipCode();
                                            });
            var location = locationFaker.Generate(6);


            //Testee
            var testeeFaker = new Faker<Testee>()
                .Rules((f, t) =>
                {
                    t.FirstName = f.Name.FirstName();
                    t.LastName = f.Name.LastName();
                    t.SocialSecurityNumber = f.Random.AlphaNumeric(50);
                    t.Gender = f.PickRandom<Gender>();
                    t.Email = f.Internet.Email();
                    t.BirthDate = f.Date.Between(new DateTime(1900, 4, 15), new DateTime(2005, 5, 10));

                    //Phone
                    t.PhoneNumber = new Faker<PhoneNumber>()
                                            .Rules((f, p) =>
                                            {
                                                p.AreaCode = f.Random.Int(1000, 9999);
                                                p.CountryCode = "+43"; //only austrian code
                                                p.SerialNumber = f.Random.Int(100000, 199999).ToString();
                                            });

                    // ANMELDUNGEN (0 - 5 Anmeldungen pro Testee)
                    int anzExamination = f.Random.Int(0, 5);
                    t.Examinations = new Faker<Examination>()
                        .Rules((f, e) =>
                        {
                            e.Tester = f.Random.ListItem(tester.ToList());
                            e.TestStation = f.Address.StreetAddress();
                            e.Token = f.Random.AlphaNumeric(50);
                            e.Value = f.Random.Int(0, 100).OrNull(f, 0.2f);
                            e.ScheduledTS = f.Date.Between(new DateTime(2020, 10, 15), new DateTime(2020, 10, 20));
                            e.ExaminationTS = e.ScheduledTS.AddDays(5);
                            e.Result = f.PickRandom<ExaminationResult>().OrNull(f, 0.2f);
                            e.Location = f.Random.ListItem(location.ToList());
                        })
                        .Generate(anzExamination)
                        .ToList();
                });
            // Da wir den Zunamen als Benutzername verwenden, muss dieser eindeutig sein.
            var testee = testeeFaker.Generate(20);
            Testees.AddRange(testee);
            SaveChanges();

        }

        // find a Testee by a provided Examination token
       Testee FindTesteeByExaminationToken(String token)
        {
            return Examinations.Where(e => e.Token == token).Select(e => e.Testee).FirstOrDefault();
        }

        // count all Testees of a provided Gender
        int CountTesteesByGender(Gender gender)
        {
            return Examinations.Where(e => e.Testee.Gender == gender).Count();
        }

        // find a Testee by a provided PhoneNumbers AreaCode and SerialNumber
        Testee FindTesteeByAreaCodeAndSerialNumber(int areaCode, string serialNumber)
        {
            return Testees.Where(t => t.PhoneNumber.AreaCode == areaCode && t.PhoneNumber.SerialNumber == serialNumber).FirstOrDefault();
        }

        // find all Testee by a given Examination scheduleTS
        List<Testee> FindTesteesByScheduleTS(DateTime ScheduleTS)
        {
            return Examinations.Where(e => e.ScheduledTS == ScheduleTS).Select(t => t.Testee).ToList();
        }
    }
}
