﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam_SIMA.api.Service;
using Exam_SIMA.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exam_SIMA.Controllers
{
    // Task 2 - Business Logic als Controller aufrufbar und testbar
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticsController : ControllerBase
    {
        private readonly StatisticService _service;
        private readonly TestungContext _context;

        public StatisticsController(TestungContext context)
        {
            _context = context;
            _service = new StatisticService(_context);
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Success :)");
        }

        [HttpGet("statistic1")]
        public IActionResult GetStatistic1()
        {
          Object obj =_service.Statistic1(DateTime.Now.AddYears(-50),DateTime.Now);

            return StatusCode(201, obj);
        }

        [HttpGet("statistic2")]
        public IActionResult GetStatistic2()
        {
            Object obj = _service.Statistic2(DateTime.Now.AddYears(-50), DateTime.Now);

            return StatusCode(201, obj);
        }

        [HttpGet("statistic3")]
        public IActionResult GetStatistic3()
        {
            Object obj = _service.Statistic3(DateTime.Now.AddYears(-50), DateTime.Now);

            return StatusCode(201, obj);
        }

    }
}