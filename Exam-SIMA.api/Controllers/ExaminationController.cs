﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Exam_SIMA.Domain;
using Exam_SIMA.Infrastructure;
using Exam_SIMA.Dto;
using Microsoft.EntityFrameworkCore;
using Bogus;

namespace Exam_SIMA
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExaminationController : ControllerBase
    {

        private readonly TestungContext _context;

        public ExaminationController(TestungContext context)
        {
            _context = context;
        }



        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Application started successfully! :)");
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public ActionResult<RegisterBackDto> Register([FromBody] RegisterDto register)
        {
            // ob man einen neuen User erstellen muss oder man nur der User nur erneut testen will
            Testee testee = _context.Testees.Where(t => t.SocialSecurityNumber == register.SocialSecurityNumber).Include(s => s.Examinations).FirstOrDefault();
            
            // Daten werden generiert
            string token = new Faker().Random.AlphaNumeric(50);
            Location location = new Faker<Location>()
                                         .Rules((f, l) =>
                                         {
                                             l.Country = "Austria"; //only austria
                                            l.City = f.Address.City();
                                             l.StreetNo = f.Address.StreetName();
                                             l.ZipCode = f.Address.ZipCode();
                                         });
            Examination examination = new Faker<Examination>()
                     .Rules((f, e) =>
                     {
                         e.Tester = f.Random.ListItem(_context.Testers.ToList());
                         e.TestStation = f.Address.StreetAddress();
                         e.Token = token;
                         e.Value = 0;
                         e.ScheduledTS = DateTime.Parse(register.PreferredDate);
                         e.Result = null;
                         e.Location = location;
                     });

            // erstellt eine neue Examination bei dem vorhandenen Benutzer
            if (testee != null)
            {
                testee.Examinations.Add(examination);
                _context.Testees.Update(testee);
               
                try
                {
                    _context.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    // Hier landet man, wenn Contraints fehlschlagen. Möchte man diese feiner
                    // abprüfen, so müssen sie im Programmcode vorab geprüft werden.
                    return Conflict();
                }

                return StatusCode(201, new RegisterBackDto()
                {
                    Location = location.getFullLocation(),
                    ScheduleTS = register.PreferredDate,
                    Token = token
                });
            }


            //erstellt einen neuen Benutzer falls keiner vorhanden 
            Testee t = new Testee()
            {
                BirthDate = DateTime.Parse(register.Birthdate),
                Email = register.Email,
                FirstName = register.FirstName,
                LastName = register.LastName,
                SocialSecurityNumber = register.SocialSecurityNumber,

                //new Phone
                PhoneNumber = new PhoneNumber()
                {
                    CountryCode = register.MobilePhone.Split(" ")[0],
                    AreaCode = int.Parse(register.MobilePhone.Split(" ")[1]),
                    SerialNumber = register.MobilePhone.Split(" ")[2]
                },

                //new Examination
                Examinations = new List<Examination>()
                {
                     examination
                }

            };


            _context.Testees.Add(t);
            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                // Hier landet man, wenn Contraints fehlschlagen. Möchte man diese feiner
                // abprüfen, so müssen sie im Programmcode vorab geprüft werden.
                return Conflict();
            }

            return StatusCode(201, new RegisterBackDto()
            {
                Location = location.getFullLocation(),
                ScheduleTS = register.PreferredDate,
                Token = token
            });
        }


        [HttpPut("{token}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public ActionResult<TestingBackDto> Testing(string token, [FromBody] TestingDto test)
        {
            Examination examination = _context.Examinations.Where(e => e.Token == token).Include(s => s.Location).FirstOrDefault();
            Testee testee = _context.Testees.Where(t => t.SocialSecurityNumber == test.SocialSecurityNumber && t.BirthDate == DateTime.Parse(test.Birthdate)).FirstOrDefault();

            if (examination == null || testee == null)
                return Conflict();

            try
            {
                examination.Value = test.Value;
                examination.Result = (ExaminationResult?)Enum.Parse(typeof(ExaminationResult), test.Result);
                examination.ExaminationTS = DateTime.Now;

                _context.Examinations.Update(examination);
                _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                // Hier landet man, wenn Contraints fehlschlagen. Möchte man diese feiner
                // abprüfen, so müssen sie im Programmcode vorab geprüft werden.
                return Conflict();
            }

            return StatusCode(200, new TestingBackDto()
            {
                ScheduleTS = examination.ScheduledTS.ToLongDateString(),
                SocialSecurityNumber = testee.SocialSecurityNumber,
                FirstName = testee.FirstName,
                LastName = testee.LastName,
                Birthdate = testee.BirthDate.ToLongTimeString(),
                MobilePhone = testee.PhoneNumber.getFullPhone(),
                Email = testee.Email,
                Token = examination.Token,
                Location = examination.Location.getFullLocation(),
                TestStation = examination.TestStation,
                ExaminationTS = examination.ExaminationTS.ToString(),
                Result = examination.Result.ToString(),
                Value = examination.Value,
            }); ;

        }

        [HttpGet("{socialSecurityNumber}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public ActionResult<PinDto> Reporting1(string socialSecurityNumber)
        {
            Testee testee = _context.Testees.Where(t => t.SocialSecurityNumber == socialSecurityNumber).FirstOrDefault();

            if (testee == null)
                return Conflict();

            try
            {
                testee.CurrentPin = new Faker().Random.Number(1000,9999);

                _context.Testees.Update(testee);
                _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                // Hier landet man, wenn Contraints fehlschlagen. Möchte man diese feiner
                // abprüfen, so müssen sie im Programmcode vorab geprüft werden.
                return Conflict();
            }

            return StatusCode(200, new PinDto()
            {
                pin = testee.CurrentPin.ToString()
            }) ; 

        }

        [HttpGet("{socialSecurityNumber}/{pin}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public ActionResult<ExaminationsDto> Reporting2(string socialSecurityNumber, string pin)
        {
            Testee testee = _context.Testees.Where(t => t.SocialSecurityNumber == socialSecurityNumber && t.CurrentPin ==int.Parse(pin)).FirstOrDefault();
            List<Examination> examination = _context.Examinations.Where(e => e.Testee == testee).Include(s => s.Location).ToList();

            if (testee == null)
                return Conflict();

            List<TestingBackDto> examinations = new List<TestingBackDto>();
            examination.ForEach(e =>
            {
                examinations.Add(new TestingBackDto()
                {
                    ScheduleTS = e.ScheduledTS.ToLongDateString(),
                    SocialSecurityNumber = testee.SocialSecurityNumber,
                    FirstName = testee.FirstName,
                    LastName = testee.LastName,
                    Birthdate = testee.BirthDate.ToLongTimeString(),
                    MobilePhone = testee.PhoneNumber.getFullPhone(),
                    Email = testee.Email,
                    Token = e.Token,
                    Location = e.Location.getFullLocation(),
                    TestStation = e.TestStation,
                    ExaminationTS = e.ExaminationTS.ToString(),
                    Result = e.Result.ToString(),
                    Value = e.Value,
                });
            });
            
            return StatusCode(200, new ExaminationsDto()
            {
                Examinations = examinations
            });

        }
    }
}