﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam_SIMA.Dto
{
    public class RegisterBackDto
    {
        public string ScheduleTS { get; set;}
        public string Token { get; set; }
        public string Location { get; set; }
    }
}
