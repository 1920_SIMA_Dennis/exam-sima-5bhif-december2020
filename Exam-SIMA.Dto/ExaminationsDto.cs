﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam_SIMA.Dto
{
    public class ExaminationsDto
    {
        public List<TestingBackDto> Examinations { get; set; }
    }
}
