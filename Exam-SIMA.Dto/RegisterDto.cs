﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam_SIMA.Dto
{
    public class RegisterDto
    {
        public string PreferredDate { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string Birthdate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }

    }
}
