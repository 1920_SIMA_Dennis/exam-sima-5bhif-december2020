﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam_SIMA.Dto
{
    public class TestingBackDto
    {
        public string ScheduleTS { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Birthdate { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string Location { get; set; }
        public string TestStation { get; set; }
        public string ExaminationTS { get; set; }
        public string Result { get; set; }
        public int? Value { get; set; }
    }
}
