﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam_SIMA.Dto
{
    public class TestingDto
    {
        public string ExaminationTS { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string Birthdate { get; set; }
        public string Result { get; set; }
        public int Value { get; set; }
    }
}
